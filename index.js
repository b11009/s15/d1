// Operators

// Assignment operators
// Basic assignment operator (=)
let assignmentNumber = 8;

// Arithmetic Assignment operators
// Addition assignment operator (+=)
assignmentNumber = assignmentNumber + 2;
console.log ("Result of addition assignment operator: " + assignmentNumber);

assignmentNumber += 2;
console.log ("Result of addition assignment operator: " + assignmentNumber);

// Subtraction assignment operator (-=)
assignmentNumber -= 2;
console.log ("Result of subtraction assignment operator: " + assignmentNumber);

// Multiplication assigment operator (*=)
assignmentNumber = assignmentNumber * 2;
console.log ("Result of multiplication assignment operator: " + assignmentNumber);

// Division assigment operator (/=)
assignmentNumber /= 2;
console.log ("Result of division assignment operator: " + assignmentNumber);

// Arithmetic Operators
let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of difference operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y;
console.log("Result of division operator: " + quotient);

let remainder = x % y;
console.log("Result of modulo/modulus operator: " + remainder);

// Multiple Operators and Parentheses

// using mdas
let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of MDAS operation: " + mdas);

// USING PARENTHESES
let pemdas = 1 + (2-3) * (4/5);
console.log("Result of PEMDAS operation: " + pemdas);

// INCREMENT (++) and DECREMENT (--)
let z = 1;
// Pre-increment
let increment = ++z ;

console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

// Post-increment
increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " + z);

// Pre-decrement
let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

// Post-decrement
decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z);

// Type coercion
let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log("Coercion: "+ coercion);
console.log(typeof coercion); //check the type of data "typeof"

let numC = true + 1;
console.log(numC);

let numD = false + 1;
console.log(numD);

// RELATIONAL OPERATORS

// Equality operator (==)
let juan = 'juana';
console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log('juan' == juan);

// Inequality operator(!=)
console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(1 != '1'); //false
console.log('juan' != juan); //true

// <, >, <=, >=
console.log("Less than or greater than: ")
console.log(4 < 6); //true
console.log(2 > 8); //false
console.log(5 > 5); //false
console.log(5 >= 5); //true
console.log(10 <= 15); //true

// Strictly equality operator (===)
console.log("Strict equality: ")
console.log(1 === '1');

// Strictly Inequality operator (!==)
console.log("Strict inEquality: ")
console.log(1 !== '1');

// Logical operators
console.log("Logical Operators: ")
let isLegalAge = true;
let isRegistered = false;

// And Operator (&&)
// returns true if all operands are true

let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND operator: " + allRequirementsMet);

// OR Operator (||)
// returns true if one of the operands is true

let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR operator: " + someRequirementsMet);

// Selection control structures

// if, else if and else statement
// if statement = executes a statement if a specified condition is true
/*Syntax:
	if(condition){
		statement/s;
	}
*/

let numE = -1;

if(numE < 0){
	console.log("Hello");
};

let nickName = "Mattheo";

if(nickName === 'Matt'){
	console.log("Hello "+nickName);
};

// else if clause
// executes a statement if previous conditions are false and if the specified condition is true
/*Syntax:
	if(condition1) {
		statement/s;
	}
	else if(condition2) {
		statement/s;
	}
	else if(condition3) {
		statement/s;
	}
	else if(conditionN) {
		statement/s;
	}
*/

let numF = 1;

if(numE > 0){
	console.log("Hello");
}
else if (numF > 0) {
	console.log("World");
}

// else clause = executes a statement if all other conditions are not met.
/*Syntax:
	if(condition1) {
		statement/s;
	}
	else if(condition2) {
		statement/s;
	}
	else if(condition3) {
		statement/s;
	}
	else if(conditionN) {
		statement/s;
	}
	else {
		statement/s;
	}
*/
/*
	numE = -1;
	numF = 1;
*/

if(numE > 0){
	console.log("Hello");
}
else if (numF === 0) {
	console.log("World");
}
else {
	console.log("Again");}

let message = 'No Message';
console.log(message);

function determineTyphoonIntensity(windSpeed){
	if (windSpeed < 30) {
		return 'Not a typhoon yet.';
	}
	else if (windSpeed >= 31 && windSpeed <= 61) {
		return 'Tropical depression detected';
	}
	else if (windSpeed >= 62 && windSpeed <= 88){
		return 'Tropical storm detected';
	} 
	else if (windspeed >= 89 && windSpeed <= 117) {
		return 'Severe tropical storm detected';
	}
	else {
		return 'Typhoon detected';
	}
}

message = determineTyphoonIntensity(68);
console.log(message);

let department = prompt("Enter your department");

if (department === 'a' || department === 'A') {
	console.log("You are in department A")
}
else if (department === 'b' || department === 'B') {
	console.log("You are in department B")
}
else {
	console.log("Invalid department")
}

// Ternary operator
/* Syntax:
	(expression) ? ifTrue : ifFalse;
*/

let ternaryResult = (1 < 18) ? "true": "false";
console.log("Result of Ternary Operator: " + ternaryResult);

function isOfLegalAge(){
	name = 'John';
	return 'You are of the legal age limit';
}

function isUnderAge(){
	name = 'Jane';
	return 'You are under the age limit';
}

// parseInt = converts string to values into numeric types
let age = parseInt(prompt("What is your age?"));
console.log(age);

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge() ;
console.log("Result of ternary operator in functions: " + legalAge + ", " + name);

// Switch statement

/*Syntax:
	switch(expression){
		case value1:
			statement;
			break;
		case value2
			statement;
			break;
		case value3:
			statement;
			break;
		case valueN:
			statement;
			break;
		default:
			statement;
	}
*/

let day = prompt ("What day of the week is it today?").toLowerCase(); 
console.log(day);

switch(day){
	case 'sunday':
		console.log("The color of the day is red");
		break;
	case 'monday':
		console.log("The color of the day is orange");
		break;
	case 'tuesday':
		console.log("The color of the day is yellow");
		break;
	case 'wednesday':
		console.log("The color of the day is green");
		break;
	case 'thursday':
		console.log("The color of the day is blue");
		break;
	case 'friday':
		console.log("The color of the day is indigo");
		break;	
	case 'saturday':
		console.log("The color of the day is violet");
		break;
	default:
		console.log("Please input a valid day");
}

// Try-Catch-Finally Statement = test a certain code

function showIntensityAlert(windSpeed){
	try{
		// attempt to execute a code
		alert(determineTyphoonIntensity(windSpeed));
	}
	// error/err are commonly used variables by developers for storing errors
	catch (error) {
		console.log(typeof error);
		console.warn(error.message);
		// error.message = is used to access the information relating to an error object
	}
	finally {
		// continue on executing the code regardless of success or failure of code execution in the "try" block.
		alert('Intensity updates will show new alert.');
	}
}
showIntensityAlert(56);





